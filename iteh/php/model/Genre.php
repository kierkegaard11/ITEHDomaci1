<?php

class Genre {

	public $genreId;
	public $name;

	public function __construct($name, $genreId = 0) {
		$this->genreId = $genreId;
		$this->name = $name;
	}

}

?>