<?php

	include_once "DAO.php";
	include $_SERVER['DOCUMENT_ROOT']."/iteh/php/model/Book.php";

	class DAOBook extends DAO {

		private $tableName = 'book';
		
		function __construct() {
			parent::__construct();
		}

		public function populateObjectFromDBRow($dbRow) { 
			return new Book($dbRow->isbn, $dbRow->name, $dbRow->description, $dbRow->rating, $dbRow->image, $dbRow->genre_id, $dbRow->author, $dbRow->book_id);
		}

		public function getById($bookId) {
			$query = "SELECT * FROM " . $this->tableName . " WHERE book_id = $bookId";
			return $this->getResult($query)[0];
		}

		public function getAll() {
			$query = "SELECT * FROM " . $this->tableName;
			return $this->getResult($query);
		}

		public function insert($book) {
			$query = "INSERT INTO " . $this->tableName . " (isbn, name, description, rating, image, author, genre_id) VALUES ('" . $book->isbn . "', '" . $book->name . "', '" . $book->description . "', " . $book->rating . ", '" . $book->image . "', '" . $book->author . "', " . $book ->genreId . ')';
			return $this->getResult($query);
		}

		public function update($book) {
			$query = "UPDATE " . $this->tableName . " SET isbn='" . $book->isbn . "', name='" . $book->name . "', description='" . $book->description . "', rating=" . $book->rating . ", image='" . $book->image . "', author='" . $book->author . "', genre_id=" . $book ->genreId . ' WHERE book_id=' . $book->bookId;
			return $this->getResult($query);
		}

		public function delete($bookId) {
			$query = "DELETE FROM " . $this->tableName . " WHERE book_id=" . $bookId;
			return $this->dbBroker->query($query);
		}

	}

?>