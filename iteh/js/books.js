$(document).ready(function () {

    let booksUrl = './php/server/books.php';
    let genresUrl = './php/server/genres.php';
    let imagePath = './images';

    let maxNumberOfBooksPerRow = 3;

    let $pageTitle = $('.navbar-brand');
    let $searchByName = $('#searchByName');
    let $searchByRating = $('#searchByRating');
    let $faStarsSearch = $('.fa-star-search');
    let $booksContainer = $('#booksContainer');
    let $bookItems;
    let $alert = $('#alert');
    let $alertText = $('#alertText');
    let $alertBtnClose = $('#alertBtnClose');
    let $btnCreateNewBook = $('#btnCreateNewBook');
    let $bookDetailsContainer = $('#bookDetailsContainer');
    let $footerText = $('#footerText');

    let $bookId = $('#bookId');
    let $inputIsbn = $('#inputIsbn');
    let $inputName = $('#inputName');
    let $inputDescription = $('#inputDescription');
    let $inputRating = $('.fa-star-input');
    let $inputAuthor = $('#inputAuthor');
    let $inputImage = $('#inputImage');
    let $inputGenre = $('#inputGenre');

    let $errorIsbn = $('#errorIsbn');
    let $errorName = $('#errorName');
    let $errorDescription = $('#errorDescription');
    let $errorRating = $('#errorRating');
    let $errorAuthor = $('#errorAuthor');
    let $errorImage = $('#errorImage');

    let $imagePreview = $('#imagePreview');
    let $btnPreviewImage = $('#btnPreviewImage');

    let $buttonsContainer = $('#buttonsContainer');
    let $btnCreateUpdate = $('#btnCreateUpdate');
    let $btnDelete = $('#btnDelete');
    let $btnDeleteConfirm = $('#btnDeleteConfirm');
    let $btnCancel = $('#btnCancel');

    let createMode;

    function setupPage() {
        loadAllBooks();
        loadGenres();
        setupAlert();
        setupBtnCreateNewBook();
        setupOperationButtons();
        addImagePreviewListener();
        generateFooterText();
        setupSearchByName();
        setupSearchByRating();
        bindOnResize();
        setupInputRating();
    }

    function loadAllBooks() {
        $.ajax({
            type: 'GET',
            url: booksUrl,
            success: function (data) {
                var books = JSON.parse(data);
                if (books.length > 0) {
                    populateBooksContainer(books);
                    addBookItemImagesListeners();
                    $bookItems = $('.book-item');
                    toggleView(true);
                } else {
                    showAlert('warning', 'No books available...');
                }
            },
            error: function () {
                showAlert('danger', 'Error occurred while loading all books...');
            }
        });
    }

    function populateBooksContainer(books) {
        $booksContainer.html('');
        var booksHtml = '';
        for (let i = 0; i < books.length; i++) {
            if (i % maxNumberOfBooksPerRow == 0) {
                booksHtml += '</div><div class=\"row book-row\">';
            }
            booksHtml += createBookCol(books[i]);
        }
        $booksContainer.append(booksHtml);
    }

    function createBookCol(book) {
        let bookColWidth = 12 / maxNumberOfBooksPerRow;
        let bookCol = '<div id=\"' + book.bookId + '\" class=\"col-lg-' + bookColWidth + ' book-item\">';
        bookCol += createBookColData(book);
        bookCol += '</div>';
        return bookCol;
    }

    function createBookColData(book) {
        let bookColData = '';
        bookColData += createBookImage(book);
        bookColData += createBookName(book);
        bookColData += createBookRating(book);
        return bookColData;
    }

    function createBookImage(book) {
        return '<img class=\"book-item-image\" src=\"' + getFullImagePath(book.image) + '\">';
    }

    function createBookName(book) {
        return '<p class=\"book-item-name\">' + book.name + '</p>';
    }

    function createBookRating(book) {
        let bookRating = '';
        let maxNumberOfStars = 5;
        let numberOfCheckedStars = book.rating;
        let numberOfUncheckedStars = maxNumberOfStars - numberOfCheckedStars;
        for (let i = 0; i < numberOfCheckedStars; i++) {
            bookRating += '<span class=\"fa fa-star book-item-fa-star checked\"></span>';
        }
        for (let i = 0; i < numberOfUncheckedStars; i++) {
            bookRating += '<span class=\"fa fa-star book-item-fa-star\"></span>';
        }
        return bookRating;
    }

    function addBookItemImagesListeners() {
        let $bookItemImages = $('.book-item-image');
        $bookItemImages.on('click', function () {
            setPageTitle('Update book');
            let bookId = $(this).parent().attr('id');
            getBookById(bookId);
        });
    }

    function showAlert(colorClass, text) {
        $alert.attr('hidden', false);
        $alert.removeClass()
        $alert.addClass('alert alert-' + colorClass);
        $alertText.html(text);
    }

    function setupAlert() {
        $alert.attr('hidden', true);
        $alertBtnClose.on('click', function () {
            $alert.attr('hidden', true);
        });
    }

    function setupBtnCreateNewBook() {
        $btnCreateNewBook.on('click', function () {
            setPageTitle('Create new book');
            resetBookForm();
            setCreateMode(true);
            toggleView(false);
        });
    }

    function getBookById(bookId) {
        $.ajax({
            type: 'GET',
            url: booksUrl + '?bookId=' + bookId,
            success: function (data) {
                var book = JSON.parse(data);
                populateBookDetailsSection(book);
                setCreateMode(false);
                toggleView(false);
            },
            error: function () {
                showAlert('danger', 'Error occurred while loading book with bookId = ' + bookId + '...');
            }
        });
    }

    function loadGenres(book) {
        $.ajax({
            type: 'GET',
            url: genresUrl,
            success: function (data) {
                var genres = JSON.parse(data);
                populateInputGenre(genres);
                console.log(data);
            },
            error: function (data) {
                showAlert('danger', 'Error occurred while loading genres...');
                console.log(data);
            }
        });
    }

    function populateBookDetailsSection(book) {
        $bookId.val(book.bookId);
        $inputIsbn.val(book.isbn);
        $inputName.val(book.name);
        $inputDescription.val(book.description);
        populateInputRating(book.rating);
        $inputAuthor.val(book.author);
        $inputImage.val(book.image);
        $inputGenre.val(book.genreId);
        togglePreviewImage(true);
    }

    function populateInputRating(rating) {
        let shouldCheck = true;
        let numberOfStarsChecked = 0;
        $.each($inputRating, function() {
            let $faStar = $(this);
            if (shouldCheck) {
                $faStar.addClass('checked');
                numberOfStarsChecked++;
            }
            if (numberOfStarsChecked == rating) {
                shouldCheck = false;
            }
        });
    }

    function populateInputGenre(genres) {
        let genreOptions = '';
        for (let i = 0; i < genres.length; i++) {
            genreOptions += createGenreOption(genres[i]);
        }
        $inputGenre.append(genreOptions);
    }

    function createGenreOption(genre) {
        return '<option value=\"' + genre.genreId + '\">' + genre.name + '</option>';
    }

    function toggleView(isBooksView) {
        if (isBooksView) {
            setPageTitle('Books');
        }
        toggleSearch(isBooksView);
        $booksContainer.attr('hidden', !isBooksView);
        $buttonsContainer.attr('hidden', isBooksView);
        $bookDetailsContainer.attr('hidden', isBooksView);
        togglePreviewImage(!isBooksView);
    }

    function setPageTitle(pageTitle) {
        $pageTitle.html(pageTitle);
    }

    function resetBookForm() {
        resetInputs();
        resetErrors();
        togglePreviewImage(true);
    }

    function resetInputs() {
        $bookId.val(0);
        $inputIsbn.val('');
        $inputName.val('');
        $inputDescription.val('');
        resetInputRating();
        $inputAuthor.val('');
        $inputImage.val('');
        $inputGenre.prop('selectedIndex', 0);
    }

    function resetInputRating() {
        $.each($inputRating, function() {
            let $faStar = $(this);
            $faStar.removeClass('checked');
        });
    }

    function resetErrors() {
        resetError($errorIsbn);
        resetError($errorName);
        resetError($errorDescription);
        resetError($errorRating);
        resetError($errorAuthor);
        resetError($errorImage);
    }

    function resetError($error) {
        $error.val('');
        $error.attr('hidden', true);
    }

    function addImagePreviewListener() {
        $btnPreviewImage.on('click', function () {
            togglePreviewImage(true);
        });
    }

    function togglePreviewImage(show) {
        if (show) {
            $imagePreview.attr('src', getFullImagePath($inputImage.val()));
        } else {
            $imagePreview.attr('src', '');
        }
        $imagePreview.attr('hidden', !show);
    }

    function getFullImagePath(image) {
        return imagePath + '/' + image;
    }

    function setCreateMode(signal) {
        createMode = signal;
        let text = createMode ? 'Create' : 'Update';
        $btnCreateUpdate.html(text);
        $btnDelete.attr('hidden', createMode);
    }

    function setupOperationButtons() {
        $btnCreateUpdate.on('click', createUpdateBook);
        $btnDeleteConfirm.on('click', deleteBook);
        $btnCancel.on('click', cancelOperation);
    }

    function createUpdateBook() {
        if (validateForm()) {
            if (createMode) {
                createBook();
            } else {
                updateBook();
            }
        }
    }

    function validateForm() {
        return validateInputNotEmpty($inputIsbn, $errorIsbn, 'Isbn') &
            validateInputNotEmpty($inputName, $errorName, 'Name') &
            validateInputNotEmpty($inputDescription, $errorDescription, 'Description') &
            validateRating() &
            validateInputNotEmpty($inputAuthor, $errorAuthor, 'Author') &
            validateInputNotEmpty($inputImage, $errorImage, 'Image');
    }

    function validateInputNotEmpty($input, $error, field) {
        let valid = $input.val().length > 0;
        if (!valid) {
            $error.val(field + ' must not be empty');
        }
        $error.attr('hidden', valid);
        return valid;
    }

    function validateRating() {
        let valid = getNumberOfCheckedStars($inputRating) != 0;
        if (!valid) {
            $errorRating.val('Rating must not be empty');
        }
        $errorRating.attr('hidden', valid);
        return valid;
    }

    function createBook() {
        $.ajax({
            type: 'POST',
            url: booksUrl,
            data: getBookFromForm(),
            success: function () {
                showAlert('success', 'New book has been successfully created...');
                loadAllBooks();
            },
            error: function () {
                showAlert('danger', 'Error occurred while creating a new book...');
            }
        });
    }

    function updateBook() {
        $.ajax({
            type: 'PUT',
            url: booksUrl,
            data: getBookFromForm(),
            success: function (dta) {
                showAlert('success', 'Book has been successfully updated...');
                loadAllBooks();
            },
            error: function () {
                showAlert('danger', 'Error occurred while updating a book...');
            }
        });
    }

    function deleteBook() {
        $.ajax({
            type: 'DELETE',
            url: booksUrl + '?bookId=' + $bookId.val(),
            success: function () {
                showAlert('success', 'Book has been successfully deleted...');
                loadAllBooks();
            },
            error: function () {
                showAlert('danger', 'Error occurred while deleting a book...');
            }
        });
    }

    function cancelOperation() {
        toggleView(true);
    }

    function getBookFromForm() {
        return {
            bookId: $bookId.val(),
            isbn: $inputIsbn.val(),
            name: $inputName.val(),
            description: $inputDescription.val(),
            rating: getNumberOfCheckedStars($inputRating),
            author: $inputAuthor.val(),
            image: $inputImage.val(),
            genreId: $inputGenre.find(":selected").attr('value')
        };
    }

    function generateFooterText() {
        let currentDate = new Date();
        $footerText.html('Copyright © ' + currentDate.getFullYear() + ' Maja Miljanic. All Rights Reserved.');
    }

    function setupSearchByName() {
        $searchByName.on('input', function () {
            let searchText = $searchByName.val();
            $.each($bookItems, function () {
                let $bookItem = $(this);
                let bookItemName = $bookItem.find('.book-item-name').html();
                if (bookItemName.toLowerCase().includes(searchText.toLowerCase())) {
                    $bookItem.attr('hidden', false);
                } else {
                    $bookItem.attr('hidden', true);
                }
            });
        });
    }

    function setupSearchByRating() {
        $faStarsSearch.on('click', function () {
            let $clickedFaStarSearch = $(this);
            if (sameAsFaStarClicked($clickedFaStarSearch, $faStarsSearch)) {
                resetFaStars($faStarsSearch);
                resetBooks();
            } else {
                checkFaStarsExcept($clickedFaStarSearch, $faStarsSearch);
                searchByRating($clickedFaStarSearch);
            }
        });

        addMouseEnterListenerToFaStars($faStarsSearch);
    }

    function sameAsFaStarClicked($clickedFaStar, $faStars) {
        return getFaStarNumber($clickedFaStar, $faStars) == getNumberOfCheckedStars($faStars);
    }

    function getFaStarNumber($clickedFaStar, $faStars) {
        let count = true;
        let faStarNumber = 0;
        $.each($faStars, function() {
            let $faStar = $(this);
            if (count) {
                faStarNumber++;
            }
            if ($faStar.attr('id') == $clickedFaStar.attr('id')) {
                count = false;
            }
        });
        return faStarNumber;
    }

    function getNumberOfCheckedStars($faStars) {
        let numberOfCheckedStars = 0;
        $.each($faStars, function() {
            let $faStar = $(this);
            if ($faStar.hasClass('checked')) {
                numberOfCheckedStars++;
            }
        });
        return numberOfCheckedStars;
    }

    function resetFaStars($faStars) {
         $.each($faStars, function () {
            let $faStar = $(this);
            $faStar.removeClass('checked');
        });
    }

    function resetBooks() {
        $.each($bookItems, function () {
            let $bookItem = $(this);
            $bookItem.attr('hidden', false);
        });
    }

    function checkFaStarsExcept($excludedFaStar, $faStars) {
        let shouldCheck = true;
        $.each($faStars, function () {
            let $faStar = $(this);
            if (shouldCheck) {
                $faStar.addClass('checked');
            } else {
                $faStar.removeClass('checked');
            }
            if ($faStar.attr('id') == $excludedFaStar.attr('id')) {
                shouldCheck = false;
            }
        });
    }

    function searchByRating($clickedFaStarSearch) {
        let rating = $clickedFaStarSearch.attr('value');
        $.each($bookItems, function () {
            let $bookItem = $(this);
            let bookItemRating = getBookItemRating($bookItem);
            if (bookItemRating == rating) {
                $bookItem.attr('hidden', false);
            } else {
                $bookItem.attr('hidden', true);
            }
        });
    }

    function getBookItemRating($bookItem) {
        return $bookItem.find('.checked').length;
    }

    function toggleSearch(show) {
        $searchByName.attr('hidden', !show);
        $searchByRating.attr('hidden', !show);
    }

    function bindOnResize() {
        $(window).bind("resize", function () {
            if ($(this).width() < 992) {
                $booksContainer.addClass('big-margin-top');
                $bookDetailsContainer.addClass('big-margin-top');
                $btnPreviewImage.addClass('small-margin-top');
            } else {
                $booksContainer.removeClass('big-margin-top');
                $bookDetailsContainer.removeClass('big-margin-top');
                $btnPreviewImage.removeClass('small-margin-top');
            }
        }).trigger('resize');
    }

    function setupInputRating() {
        $inputRating.on('click', function () {
            let $clickedFaStarInput = $(this);
            if (sameAsFaStarClicked($clickedFaStarInput, $inputRating)) {
                resetFaStars($inputRating);
            } else {
                checkFaStarsExcept($clickedFaStarInput, $inputRating);
            }
        });

        addMouseEnterListenerToFaStars($inputRating);
    }

    function addMouseEnterListenerToFaStars($faStars) {
        $faStars.on('mouseenter', function () {
            let $faStar = $(this);
            $faStar.addClass('pointer');
        });
    }

    setupPage();

});