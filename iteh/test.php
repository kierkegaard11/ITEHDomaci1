<?php
	include_once $_SERVER['DOCUMENT_ROOT'] . "/iteh/php/dao/DAOGenre.php";

	if ($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['genreId'])) {
        $dao = new DAOGenre();
        $genre = $dao->getById($_GET['genreId']);
        echo "{\"name\": \"" . $genre->getName() . "\", \"description\": \"" . $genre->getDescription() . "\"}";
	}
	
?>